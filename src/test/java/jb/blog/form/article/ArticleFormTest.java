package jb.blog.form.article;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ArticleFormTest {
    @Test
    void validate_shouldValidateCorrectForm() {
        ArticleForm form = new ArticleFormImpl();
        form.setContent("valid content");
        form.setTitle("valid title");

        boolean valid = form.validate();
        assertTrue(valid);
    }
}
