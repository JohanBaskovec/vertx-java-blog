package jb.blog.form.article;

import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArticleFormFactoryTest {
	@Test
	void fromJsonObject_should_returnArticleForm() {
		ArticleFormFactory factory = new ArticleFormFactoryImpl();
		String title = "test title";
		String content = "test content";
		JsonObject json = new JsonObject()
				.put("title", title)
				.put("content", content);

		ArticleForm form = factory.fromJsonObject(json);

		assertEquals(title, form.getTitle());
		assertEquals(content, form.getContent());
	}
}
