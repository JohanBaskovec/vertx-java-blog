package jb.blog.integration;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.web.client.WebClient;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class LoginTest {
	private static final Vertx vertx = Vertx.vertx();
	private VertxTestContext testContext;
	private static String deploymentId;

	@BeforeAll
	static void beforeAll() {
		System.setProperty("resetDatabase", "true");
		deploymentId = vertx.rxDeployVerticle("jb.blog.Application").blockingGet();
	}

	@AfterAll
	static void afterAll() {
		//noinspection ResultOfMethodCallIgnored
		vertx.rxUndeploy(deploymentId).blockingAwait(5, TimeUnit.SECONDS);
	}

	@BeforeEach
	void beforeEach() {
		testContext = new VertxTestContext();
	}

	@Test
	void loginWithCorrectUsernameAndPasswordShouldSendSessionCookie() throws Throwable {
		WebClient client = WebClient.create(vertx, new WebClientOptions());
		//noinspection ResultOfMethodCallIgnored
		client.post(8092, "localhost", "/session")
				.rxSendJson(new JsonObject()
						.put("username", "username_1")
						.put("password", "password_1"))
				.subscribe(response -> testContext.verify(() -> {
							assertEquals(200, response.statusCode());
							// this is the default cookie name defined in
							// io.vertx.ext.web.handler.SessionHandler
							assertNotNull(response.getHeader("set-cookie"));
							testContext.completeNow();
						}
				));
		testContext.awaitCompletion(3, TimeUnit.SECONDS);
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}

	}

	@Test
	void loginWithIncorrectUsernameShouldSend403Response() throws Throwable {
		WebClient client = WebClient.create(vertx, new WebClientOptions());
		//noinspection ResultOfMethodCallIgnored
		client.post(8092, "localhost", "/session")
				.rxSendJson(new JsonObject()
						.put("username", "username_wrong")
						.put("password", "password_1"))
				.subscribe(response -> testContext.verify(() -> {
					assertEquals(403, response.statusCode());
					assertNull(response.getHeader("set-cookie"));
					testContext.completeNow();
				}));
		testContext.awaitCompletion(3, TimeUnit.SECONDS);
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}
}
