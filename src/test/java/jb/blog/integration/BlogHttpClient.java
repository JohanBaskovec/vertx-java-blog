package jb.blog.integration;

import io.reactivex.Completable;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.RequestOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.ext.web.client.HttpRequest;
import io.vertx.reactivex.ext.web.client.WebClient;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class BlogHttpClient {
	private WebClient client;
	private String sessionCookieHeader;

	BlogHttpClient(Vertx vertx, WebClientOptions options) {
		client = WebClient.create(vertx, options);
	}

	Completable login(String username, String password) {
		return post(8092, "localhost", "/session")
				.rxSendJson(new JsonObject()
						.put("username", username)
						.put("password", password))
				.flatMapCompletable(response -> Completable.fromAction(() -> {
							String setCookieString = response.getHeader("set-cookie");
							Pattern setCookiePattern = Pattern.compile("(vertx-web\\.session=.*);.*");
							Matcher matcher = setCookiePattern.matcher(setCookieString);
							if (matcher.matches()) {
								sessionCookieHeader = matcher.group(1);
							}
						})
				);
	}

	private HttpRequest<Buffer> setSessionCookieHeaderIfLoggedIn(HttpRequest<Buffer> request) {
		if (sessionCookieHeader != null) {
			request.putHeader("Cookie", sessionCookieHeader);
		}
		return request;
	}

	io.vertx.ext.web.client.WebClient getDelegate() {
		return client.getDelegate();
	}

	HttpRequest<Buffer> request(HttpMethod method, int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.request(method, port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> request(HttpMethod method, String host, String requestURI) {
		HttpRequest<Buffer> request = client.request(method, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> request(HttpMethod method, String requestURI) {
		HttpRequest<Buffer> request = client.request(method, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> request(HttpMethod method, RequestOptions options) {
		HttpRequest<Buffer> request = client.request(method, options);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> requestAbs(HttpMethod method, String absoluteURI) {
		HttpRequest<Buffer> request = client.requestAbs(method, absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> get(String requestURI) {
		HttpRequest<Buffer> request = client.get(requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> get(int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.get(port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> get(String host, String requestURI) {
		HttpRequest<Buffer> request = client.get(host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> getAbs(String absoluteURI) {
		HttpRequest<Buffer> request = client.getAbs(absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> post(String requestURI) {
		HttpRequest<Buffer> request = client.post(requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> post(int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.post(port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> post(String host, String requestURI) {
		HttpRequest<Buffer> request = client.post(host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> postAbs(String absoluteURI) {
		HttpRequest<Buffer> request = client.postAbs(absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> put(String requestURI) {
		HttpRequest<Buffer> request = client.put(requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> put(int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.put(port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> put(String host, String requestURI) {
		HttpRequest<Buffer> request = client.put(host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> putAbs(String absoluteURI) {
		HttpRequest<Buffer> request = client.putAbs(absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> delete(String requestURI) {
		HttpRequest<Buffer> request = client.delete(requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> delete(int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.delete(port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> delete(String host, String requestURI) {
		HttpRequest<Buffer> request = client.delete(host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> deleteAbs(String absoluteURI) {
		HttpRequest<Buffer> request = client.deleteAbs(absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> patch(String requestURI) {
		HttpRequest<Buffer> request = client.patch(requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> patch(int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.patch(port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> patch(String host, String requestURI) {
		HttpRequest<Buffer> request = client.patch(host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> patchAbs(String absoluteURI) {
		HttpRequest<Buffer> request = client.patchAbs(absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> head(String requestURI) {
		HttpRequest<Buffer> request = client.head(requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> head(int port, String host, String requestURI) {
		HttpRequest<Buffer> request = client.head(port, host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> head(String host, String requestURI) {
		HttpRequest<Buffer> request = client.head(host, requestURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	HttpRequest<Buffer> headAbs(String absoluteURI) {
		HttpRequest<Buffer> request = client.headAbs(absoluteURI);
		return setSessionCookieHeaderIfLoggedIn(request);
	}

	void close() {
		client.close();
	}
}
