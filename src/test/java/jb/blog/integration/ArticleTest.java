package jb.blog.integration;

import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import jb.blog.model.article.Article;
import jb.blog.repository.article.ArticleRepository;
import jb.blog.repository.article.ArticleRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(VertxExtension.class)
class ArticleTest extends IntegrationTest {
	@Test
	void gettingAllArticles_should_returnAllArticles() throws Throwable {
		//noinspection ResultOfMethodCallIgnored
		httpClient.get(8092, "localhost", "/article")
				.rxSend()
				.subscribe(
						response -> testContext.verify(() -> {
							JsonObject jsonResponse = response.bodyAsJsonObject();
							JsonArray data = jsonResponse.getJsonArray("data");
							assertNotNull(data);
							int arraySize = data.size();
							assertEquals(3, arraySize);
							testContext.completeNow();
						})
				);

		assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}

	@Test
	void gettingOneArticle() throws Throwable {
		//noinspection ResultOfMethodCallIgnored
		httpClient.get(8092, "localhost", "/article/2")
				.rxSend()
				.subscribe(
						response -> testContext.verify(() -> {
							JsonObject jsonResponse = response.bodyAsJsonObject();
							JsonObject data = jsonResponse.getJsonObject("data");
							assertNotNull(data);
							String title = data.getString("title");
							String content = data.getString("content");
							assertEquals("title 2", title);
							assertEquals("content 2", content);
							testContext.completeNow();
						})
				);

		assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}

	@Test
	void postingArticleWithoutBeingLoggedInShouldReturn401HttpStatus() throws Throwable {
		//noinspection ResultOfMethodCallIgnored
		httpClient.post(8092, "localhost", "/article")
				.rxSendJson(new JsonObject()
						.put("title", "post title")
						.put("content", "post content")
				)
				.subscribe(
						response -> testContext.verify(() -> {
							assertEquals(401, response.statusCode());
							testContext.completeNow();
						}),
						error -> testContext.failNow(error)
				);

		assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}

	@Test
	void postingArticleWhenLoggedIn_should_createTheArticle() throws Throwable {
		//noinspection ResultOfMethodCallIgnored
		httpClient.login("username_1", "password_1")
				.andThen(Single.defer(() ->
						httpClient.post(8092, "localhost", "/article")
								.rxSendJson(new JsonObject()
										.put("title", "super title")
										.put("content", "content")
								)
				))
				.flatMap(response -> {
					testContext.verify(() -> assertEquals(200, response.statusCode()));
					ArticleRepository articleRepository = new ArticleRepositoryImpl(sqlConnection);
					return articleRepository.findByTitle("super title");
				})
				.subscribe(articles -> testContext.verify(() -> {
					assertEquals(1, articles.size());
					Article article = articles.get(0);
					assertEquals("content", article.getContent());
					assertEquals("super title", article.getTitle());
					testContext.completeNow();
				}));

		assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}

	@Test
	void attemptingToModifyArticleWithoutBeingLoggedInShouldReturn401HttpStatus() throws Throwable {
		//noinspection ResultOfMethodCallIgnored
		httpClient.put(8092, "localhost", "/article/2")
				.rxSendJson(new JsonObject()
						.put("title", "post title")
						.put("content", "post content")
				)
				.subscribe(
						response -> testContext.verify(() -> {
							assertEquals(401, response.statusCode());
							testContext.completeNow();
						}),
						error -> testContext.failNow(error)
				);

		assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}

	@Test
	void attemptingToModifyArticleWhenLoggedInShouldModifyTheArticle() throws Throwable {
		//noinspection ResultOfMethodCallIgnored
		httpClient.login("username_1", "password_1")
				.andThen(Single.defer(() ->
						httpClient.put(8092, "localhost", "/article/2")
								.rxSendJson(new JsonObject()
										.put("title", "new title")
										.put("content", "new content")
								)
				))
				.flatMap(response -> {
					testContext.verify(() -> assertEquals(204, response.statusCode()));
					ArticleRepository articleRepository = new ArticleRepositoryImpl(sqlConnection);
					return articleRepository.findByTitle("new title");
				})
				.subscribe(articles -> testContext.verify(() -> {
					assertEquals(1, articles.size());
					Article article = articles.get(0);
					assertEquals("new content", article.getContent());
					assertEquals("new title", article.getTitle());
					testContext.completeNow();
				}));

		assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
		if (testContext.failed()) {
			throw testContext.causeOfFailure();
		}
	}
}
