package jb.blog.integration;

import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.asyncsql.PostgreSQLClient;
import io.vertx.reactivex.ext.sql.SQLClient;
import io.vertx.reactivex.ext.sql.SQLConnection;
import jb.blog.service.DatabaseReinitializerService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.concurrent.TimeUnit;

class IntegrationTest {
	private static final Vertx vertx = Vertx.vertx();
	VertxTestContext testContext;
	private static String deploymentId;
	BlogHttpClient httpClient;
	private static ConfigRetriever retriever;
	SQLConnection sqlConnection;
	private SQLClient sqlClient;

	@BeforeAll
	static void beforeAll() {
		ConfigStoreOptions store = new ConfigStoreOptions()
				.setType("file")
				.setFormat("yaml")
				.setConfig(new JsonObject().put("path", "src/test/resources/config.yml"));
		retriever = ConfigRetriever.create(
				vertx,
				new ConfigRetrieverOptions().addStore(store)
		);
		System.setProperty("resetDatabase", "true");
		deploymentId = vertx.rxDeployVerticle("jb.blog.Application").blockingGet();
	}

	@BeforeEach
	void beforeEach() {
		testContext = new VertxTestContext();
		httpClient = new BlogHttpClient(vertx, new WebClientOptions());

		retriever.rxGetConfig()
				.flatMap(config -> {
					sqlClient = PostgreSQLClient.createShared(
							vertx,
							config,
							"PostgresPool1"
					);
					return sqlClient.rxGetConnection();
				})
				.flatMapCompletable(sqlConnection -> {
					IntegrationTest.this.sqlConnection = sqlConnection;
					DatabaseReinitializerService dbReinitializationService = new DatabaseReinitializerService(sqlConnection);
					return dbReinitializationService.reinitialize();
				})
				.blockingAwait();
	}

	@AfterAll
	static void afterAll() {
		//noinspection ResultOfMethodCallIgnored
		vertx.rxUndeploy(deploymentId).blockingAwait(5, TimeUnit.SECONDS);
	}
}
