package jb.blog.model;

import jb.blog.form.article.ArticleForm;
import jb.blog.model.article.Article;
import jb.blog.model.article.ArticleImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ArticleTest {
	@Test
	void updateFromForm_should_updateFromForm() {
		Article article = new ArticleImpl("original title", "original content", 3);
		ArticleForm form = mock(ArticleForm.class);
		when(form.getTitle()).thenReturn("test title");
		when(form.getContent()).thenReturn("test content");

		article.updateFromForm(form);

		assertEquals(form.getTitle(), article.getTitle());
		assertEquals(form.getContent(), article.getContent());
		assertEquals(3, article.getId());
	}
}
