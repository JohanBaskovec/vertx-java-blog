package jb.blog.controller.article;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.RoutingContext;
import jb.blog.form.article.ArticleForm;
import jb.blog.form.article.ArticleFormFactory;
import jb.blog.model.article.Article;
import jb.blog.model.exception.InvalidFormException;
import jb.blog.model.exception.ResourceNotFoundException;
import jb.blog.repository.article.ArticleRepository;
import jb.blog.service.Injector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ArticleControllerTest {
	private ArticleFormFactory articleFormFactory;
	private ArticleController controller;
	private RoutingContext routingContext;
	private ArticleRepository articleRepository;
	private Injector injector;

	@BeforeEach
	void beforeEach() {
		articleFormFactory = mock(ArticleFormFactory.class);
		controller = new ArticleController(articleFormFactory);
		routingContext = mock(RoutingContext.class);
		articleRepository = mock(ArticleRepository.class);
		injector = mock(Injector.class);
		when(routingContext.get("Injector")).thenReturn(injector);
		when(injector.getArticleRepository()).thenReturn(articleRepository);
	}

	@Test
	void getOne_should_returnArticleJsonWhenArticleExists() {
		Article article = mock(Article.class);
		JsonObject articleJson = new JsonObject();

		when(article.toJsonObject()).thenReturn(articleJson);
		when(articleRepository.getOne(3)).thenReturn(Maybe.just(article));
		when(routingContext.get("id")).thenReturn(3L);

		controller.getOne(routingContext);

		verify(routingContext).put(eq("data"), same(articleJson));
		verify(routingContext).next();
	}

	@Test
	void getOne_should_failWithResourceNotFoundExceptionWhenArticleDoesntExist() {
		when(articleRepository.getOne(3)).thenReturn(Maybe.empty());
		when(routingContext.get("id")).thenReturn(3L);

		controller.getOne(routingContext);

		verify(routingContext).fail(any(ResourceNotFoundException.class));
	}

	@Test
	void createOne_should_saveNewArticleAndReturnIt() {
		ArticleForm articleForm = mock(ArticleForm.class);
		Article articleBeforeSave = mock(Article.class);
		Article articleAfterSave = mock(Article.class);
		JsonObject articleAfterSaveJson = new JsonObject();
		when(articleAfterSave.toJsonObject()).thenReturn(articleAfterSaveJson);

		when(articleForm.toArticle()).thenReturn(articleBeforeSave);
		when(articleForm.validate()).thenReturn(true);
		when(articleFormFactory.fromRoutingContext(routingContext)).thenReturn(articleForm);
		when(articleRepository.save(eq(articleBeforeSave))).thenReturn(Single.just(articleAfterSave));

		controller.createOne(routingContext);

		verify(articleRepository).save(same(articleBeforeSave));
		verify(routingContext).put(eq("data"), same(articleAfterSaveJson));
		verify(routingContext).next();
	}

	@Test
	void createOne_should_throwInvalidFormExceptionWhenFormIsInvalid() {
		ArticleForm articleForm = mock(ArticleForm.class);

		when(articleForm.validate()).thenReturn(false);
		when(articleFormFactory.fromRoutingContext(routingContext)).thenReturn(articleForm);

		controller.createOne(routingContext);

		verify(routingContext).fail(any(InvalidFormException.class));
	}

	@Test
	void updateOne_should_updateArticleAndReturnUpdatedArticleWhenFormIsValid() {
		ArticleForm articleForm = mock(ArticleForm.class);
		Article articleBeforeSave = mock(Article.class);
		Article articleAfterSave = mock(Article.class);
		HttpServerResponse response = mock(HttpServerResponse.class);
		JsonObject articleAfterSaveJson = new JsonObject();
		long id = 3;

		when(routingContext.get("id")).thenReturn(id);
		when(articleRepository.getOne(id)).thenReturn(Maybe.just(articleBeforeSave));
		when(articleFormFactory.fromRoutingContext(routingContext)).thenReturn(articleForm);
		when(articleForm.validate()).thenReturn(true);
		when(articleAfterSave.toJsonObject()).thenReturn(articleAfterSaveJson);
		when(articleRepository.save(eq(articleBeforeSave))).thenReturn(Single.just(articleAfterSave));
		when(routingContext.response()).thenReturn(response);
		when(response.setStatusCode(anyInt())).thenReturn(response);

		controller.updateOne(routingContext);

		verify(articleBeforeSave).updateFromForm(articleForm);
		verify(articleRepository).save(same(articleBeforeSave));
		verify(response).setStatusCode(204);
		verify(response).end();
	}

	@Test
	void updateOne_should_throwInvalidFormExceptionWhenFormIsInvalid() {
		ArticleForm articleForm = mock(ArticleForm.class);
		Article article = mock(Article.class);
		long id = 3;
		when(routingContext.get("id")).thenReturn(id);
		when(articleRepository.getOne(id)).thenReturn(Maybe.just(article));
		when(articleFormFactory.fromRoutingContext(routingContext)).thenReturn(articleForm);
		when(articleForm.validate()).thenReturn(false);

		controller.updateOne(routingContext);

		verify(routingContext).fail(any(InvalidFormException.class));
	}

	@Test
	void updateOne_should_throwResourceNotFoundExceptionWhenArticleDoesNotExist() {
		ArticleForm articleForm = mock(ArticleForm.class);
		long id = 3;
		when(articleFormFactory.fromRoutingContext(routingContext)).thenReturn(articleForm);
		when(articleForm.validate()).thenReturn(true);
		when(routingContext.get("id")).thenReturn(id);
		when(articleRepository.getOne(id)).thenReturn(Maybe.empty());

		controller.updateOne(routingContext);

		verify(routingContext).fail(any(ResourceNotFoundException.class));
	}
}
