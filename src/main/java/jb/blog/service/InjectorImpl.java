package jb.blog.service;

import io.vertx.reactivex.ext.auth.AuthProvider;
import io.vertx.reactivex.ext.sql.SQLConnection;
import jb.blog.repository.article.ArticleRepository;
import jb.blog.repository.article.ArticleRepositoryImpl;
import jb.blog.repository.user.UserRepository;
import jb.blog.repository.user.UserRepositoryImpl;
import jb.blog.security.SqlAuthProviderImpl;

public class InjectorImpl implements Injector {
	private UserRepository userRepository;
	private ArticleRepository articleRepository;
	private AuthProvider authProvider;
	private SQLConnection sqlConnection;

	@Override
	public void setSqlConnection(SQLConnection sqlConnection) {
		this.sqlConnection = sqlConnection;
	}

	@Override
	public ArticleRepository getArticleRepository() {
		if (articleRepository == null) {
			articleRepository = new ArticleRepositoryImpl(sqlConnection);
		}
		return articleRepository;
	}

	@Override
	public UserRepository getUserRepository() {
		if (userRepository == null) {
			userRepository = new UserRepositoryImpl(sqlConnection);
		}
		return userRepository;
	}

	@Override
	public AuthProvider getAuthProvider() {
		if (authProvider == null) {
			authProvider = new SqlAuthProviderImpl(getUserRepository());
		}
		return authProvider;
	}

}
