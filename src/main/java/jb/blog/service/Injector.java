package jb.blog.service;

import io.vertx.reactivex.ext.auth.AuthProvider;
import io.vertx.reactivex.ext.sql.SQLConnection;
import jb.blog.repository.article.ArticleRepository;
import jb.blog.repository.user.UserRepository;

public interface Injector {
	void setSqlConnection(SQLConnection sqlConnection);

	ArticleRepository getArticleRepository();

	UserRepository getUserRepository();

	AuthProvider getAuthProvider();
}
