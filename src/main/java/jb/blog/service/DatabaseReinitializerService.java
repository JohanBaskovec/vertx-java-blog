package jb.blog.service;

import io.reactivex.Completable;
import io.vertx.reactivex.ext.sql.SQLConnection;
import jb.blog.model.article.ArticleImpl;
import jb.blog.model.user.BlogUserImpl;
import jb.blog.model.user.RxBlogUserImpl;
import jb.blog.repository.article.ArticleRepository;
import jb.blog.repository.article.ArticleRepositoryImpl;
import jb.blog.repository.user.UserRepository;
import jb.blog.repository.user.UserRepositoryImpl;

public class DatabaseReinitializerService {
	private SQLConnection sqlConnection;

	public DatabaseReinitializerService(SQLConnection sqlConnection) {
		this.sqlConnection = sqlConnection;
	}

	public Completable reinitialize() {
		ArticleRepository articleRepository = new ArticleRepositoryImpl(sqlConnection);
		UserRepository userRepository = new UserRepositoryImpl(sqlConnection);
		return sqlConnection
				.rxExecute("drop table if exists article;")
				.concatWith(
						sqlConnection.rxExecute("drop table if exists article;")
				)
				.concatWith(
						sqlConnection.rxExecute("drop table if exists blog_user;")
				)
				.concatWith(
						sqlConnection.rxExecute(
								"create table article (" +
										"  id serial primary key," +
										"  title text default ''," +
										"  content text default ''" +
										");"
						)
				)
				.concatWith(
						sqlConnection.rxExecute(
								"create table blog_user (" +
										"  id serial primary key," +
										"  username text," +
										"  password text" +
										");"
						)
				)
				.andThen(
						articleRepository.save(
								new ArticleImpl("title 1", "content 1")
						)
				)
				.concatWith(
						articleRepository.save(
								new ArticleImpl("title 2", "content 2")
						)
				)
				.concatWith(
						articleRepository.save(
								new ArticleImpl("title 3", "content 3")
						)
				)
				.ignoreElements()
				.andThen(
						userRepository.save(
								new RxBlogUserImpl(
										new BlogUserImpl("username_1", "password_1"))
						)
				)
				.concatWith(
						userRepository.save(
								new RxBlogUserImpl(
										new BlogUserImpl("username_2", "password_2")
								)
						)
				)
				.ignoreElements()
				.doFinally(sqlConnection::close)
				;
	}
}
