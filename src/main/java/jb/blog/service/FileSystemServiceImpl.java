package jb.blog.service;

import com.google.common.base.Charsets;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileSystemServiceImpl {
	public String readFileAsString(String path) throws IOException {
		return readFileAsString(path, Charsets.UTF_8);
	}

	public String readFileAsString(String path, Charset charset) throws IOException {
		try {
			return new String(Files.readAllBytes(Paths.get(path)), charset);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}
}
