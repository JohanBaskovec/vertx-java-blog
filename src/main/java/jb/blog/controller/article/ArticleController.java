package jb.blog.controller.article;

import io.vertx.core.json.JsonArray;
import io.vertx.reactivex.ext.web.RoutingContext;
import jb.blog.form.article.ArticleForm;
import jb.blog.form.article.ArticleFormFactory;
import jb.blog.model.article.Article;
import jb.blog.model.exception.InvalidFormException;
import jb.blog.model.exception.ResourceNotFoundException;
import jb.blog.observer.ApiMaybeObserver;
import jb.blog.observer.ApiSingleObserver;
import jb.blog.repository.article.ArticleRepository;
import jb.blog.service.Injector;

import java.util.List;

public class ArticleController {
	private final ArticleFormFactory articleFormFactory;

	public ArticleController(ArticleFormFactory articleFormFactory) {
		this.articleFormFactory = articleFormFactory;
	}

	public void getMultiple(RoutingContext routingContext) {
		Injector injector = routingContext.get("Injector");
		ArticleRepository articleRepository = injector.getArticleRepository();
		articleRepository
				.getAll()
				.subscribe(
						new ApiSingleObserver<List<Article>>(routingContext) {
							@Override
							public void onSuccess(List<Article> articles) {
								JsonArray data = new JsonArray();
								for (Article article : articles) {
									data.add(article.toJsonObject());
								}
								routingContext.put("data", data);
								routingContext.next();
							}
						}
				);
	}

	public void getOne(RoutingContext routingContext) {
		Injector injector = routingContext.get("Injector");
		ArticleRepository articleRepository = injector.getArticleRepository();
		long id = routingContext.get("id");
		articleRepository.getOne(id)
				.subscribe(
						new ApiMaybeObserver<Article>(routingContext) {
							@Override
							public void onSuccess(Article article) {
								routingContext.put("data", article.toJsonObject());
								routingContext.next();
							}

							@Override
							public void onComplete() {
								routingContext.fail(new ResourceNotFoundException("Article"));
							}
						}
				);
	}

	public void createOne(RoutingContext routingContext) {
		Injector injector = routingContext.get("Injector");
		ArticleRepository articleRepository = injector.getArticleRepository();
		ArticleForm form = articleFormFactory.fromRoutingContext(routingContext);
		if (form.validate()) {
			articleRepository.save(form.toArticle())
					.subscribe(
							new ApiSingleObserver<Article>(routingContext) {
								@Override
								public void onSuccess(Article article) {
									routingContext.put("data", article.toJsonObject());
									routingContext.next();
								}
							}
					);
		} else {
			routingContext.fail(new InvalidFormException(form));
		}
	}

	public void updateOne(RoutingContext routingContext) {
		ArticleForm form = articleFormFactory.fromRoutingContext(routingContext);
		if (!form.validate()) {
			routingContext.fail(new InvalidFormException(form));
			return;
		}

		Injector injector = routingContext.get("Injector");
		ArticleRepository articleRepository = injector.getArticleRepository();
		long id = routingContext.get("id");
		articleRepository.getOne(id).subscribe(new ApiMaybeObserver<Article>(routingContext) {
			@Override
			public void onSuccess(Article article) {
				article.updateFromForm(form);
				articleRepository.save(article)
						.subscribe(
								new ApiSingleObserver<Article>(routingContext) {
									@Override
									public void onSuccess(Article article) {
										routingContext.response().setStatusCode(204).end();
									}
								}
						);
			}

			@Override
			public void onComplete() {
				routingContext.fail(new ResourceNotFoundException("article"));
			}
		});
	}
}
