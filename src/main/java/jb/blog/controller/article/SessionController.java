package jb.blog.controller.article;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.auth.AuthProvider;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.Session;
import jb.blog.model.exception.InvalidUsernameOrPasswordException;
import jb.blog.model.user.RxBlogUser;
import jb.blog.service.Injector;

import java.util.NoSuchElementException;

public class SessionController {
	public void login(RoutingContext routingContext) {
		Injector injector = routingContext.get("Injector");
		AuthProvider authProvider = injector.getAuthProvider();

		JsonObject body = routingContext.getBodyAsJson();
		String username = body.getString("username");
		String password = body.getString("password");

		if (username == null || password == null) {
			routingContext.fail(new InvalidUsernameOrPasswordException());
		} else {
			Session session = routingContext.session();
			JsonObject authInfo = new JsonObject().put("username", username).put("password", password);
			//noinspection ResultOfMethodCallIgnored
			authProvider.rxAuthenticate(authInfo).subscribe(
					user -> {
						routingContext.setUser(user);
						// TODO: delete this if? Session should always exist,
						// otherwise what's the point of logging in ?
						if (session != null) {
							// the user has upgraded from unauthenticated to authenticated
							// session should be upgraded as recommended by owasp
							session.regenerateId();
						}
						RxBlogUser blogUser = (RxBlogUser) user;
						routingContext.put("data", blogUser.toJsonObject());
						routingContext.next();
					},
					err -> {
						if (err instanceof NoSuchElementException) {
							routingContext.fail(new InvalidUsernameOrPasswordException());
						} else {
							routingContext.fail(err);
						}
					}
			);
		}
	}
}
