package jb.blog.model.exception;

import io.vertx.core.json.JsonObject;

public class InvalidUsernameOrPasswordException extends ApiException {
	@Override
	public JsonObject toErrorJsonObject() {
		return new JsonObject().put("error", "Invalid username or password.");
	}

	@Override
	public int getHttpCode() {
		return 403;
	}
}
