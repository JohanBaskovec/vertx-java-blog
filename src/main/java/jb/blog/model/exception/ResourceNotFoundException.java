package jb.blog.model.exception;

import io.vertx.core.json.JsonObject;

public class ResourceNotFoundException extends ApiException {
    private final String resourceName;

    public ResourceNotFoundException(String resourceName) {
        this.resourceName = resourceName;
    }

    public JsonObject toErrorJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("error", resourceName + " not found.");
        return jsonObject;
    }

    public String getResourceName() {
        return resourceName;
    }

    @Override
    public int getHttpCode() {
        return 404;
    }
}
