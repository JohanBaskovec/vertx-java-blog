package jb.blog.model.exception;

import io.vertx.core.json.JsonObject;

public abstract class ApiException extends RuntimeException {
	public abstract JsonObject toErrorJsonObject();

	public int getHttpCode() {
		return 500;
	}
}
