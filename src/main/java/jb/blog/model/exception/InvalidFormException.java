package jb.blog.model.exception;

import io.vertx.core.json.JsonObject;
import jb.blog.form.AbstractForm;

import java.util.Map;

public class InvalidFormException extends ApiException {
	private final AbstractForm form;

	public InvalidFormException(AbstractForm form) {
		this.form = form;
	}

	@Override
	public JsonObject toErrorJsonObject() {
		JsonObject fieldErrors = new JsonObject();
		for (Map.Entry<String, String> error : form.getErrors().entrySet()) {
			fieldErrors.put(error.getKey(), error.getValue());
		}
		return new JsonObject()
				.put("errors", new JsonObject()
						.put("fields", fieldErrors));
	}
}
