package jb.blog.model.exception;

import io.vertx.core.json.JsonObject;

public class InvalidUrlParameterException extends ApiException {
	private final String parameterName;
	private final String message;

	public InvalidUrlParameterException(String parameterName, String message) {
		this.parameterName = parameterName;
		this.message = message;
	}

	@Override
	public JsonObject toErrorJsonObject() {
		return new JsonObject()
				.put("error", "Invalid URL parameter '" + parameterName + "': " + message);
	}

	@Override
	public int getHttpCode() {
		return 400;
	}
}
