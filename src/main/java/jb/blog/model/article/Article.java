package jb.blog.model.article;

import io.vertx.core.json.JsonObject;
import jb.blog.form.article.ArticleForm;

import java.util.Set;

public interface Article {
	JsonObject toJsonObject();

	JsonObject toJsonObject(Set<String> allowedFields);

	String getTitle();

	void setTitle(String title);

	String getContent();

	void setContent(String content);

	long getId();

	void setId(long id);

	void updateFromForm(ArticleForm form);
}
