package jb.blog.model.article;

import com.google.common.collect.Sets;
import io.vertx.core.json.JsonObject;
import jb.blog.form.article.ArticleForm;

import java.util.Set;

public class ArticleImpl implements Article {
    private static final Set<String> defaultAllowedFields = Sets.newHashSet(
            "title",
            "content",
            "id"
    );
    private long id;
    private String title;
    private String content;

    public ArticleImpl() {
        this("", "", 0);
    }

    public ArticleImpl(String title, String content) {
        this(title, content, 0);
    }

    public ArticleImpl(String title, String content, long id) {
        this.title = title;
        this.content = content;
        this.id = id;
    }

    @Override
    public JsonObject toJsonObject() {
        return toJsonObject(defaultAllowedFields);
    }

    @Override
    public JsonObject toJsonObject(Set<String> allowedFields) {
        JsonObject jsonObject = new JsonObject();
        if (allowedFields.contains("content")) {
            jsonObject.put("content", content);
        }
        if (allowedFields.contains("title")) {
            jsonObject.put("title", title);
        }
        if (allowedFields.contains("id")) {
            jsonObject.put("id", id);
        }
        return jsonObject;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public void updateFromForm(ArticleForm form) {
        setTitle(form.getTitle());
        setContent(form.getContent());
    }
}
