package jb.blog.model.user;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;

import java.util.Objects;

public class BlogUserImpl extends BlogUser {
	private long id;
	private String username;
	private String password;

	public BlogUserImpl() {
		this("", "");
	}

	public BlogUserImpl(String username, String password) {
		this(username, password, 0);
	}

	public BlogUserImpl(String username, String password, long id) {
		this.username = username;
		this.password = password;
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public JsonObject principal() {
		return new JsonObject();
	}

	@Override
	public void setAuthProvider(AuthProvider authProvider) {

	}

	@Override
	protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		BlogUserImpl blogUser = (BlogUserImpl) o;
		return Objects.equals(username, blogUser.username) &&
				Objects.equals(password, blogUser.password);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), username, password);
	}

	@Override
	public String toString() {
		return "BlogUserImpl{" +
				"username='" + username + '\'' +
				", password='" + password + '\'' +
				'}';
	}
}
