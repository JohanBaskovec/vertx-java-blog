package jb.blog.model.user;

import com.google.common.collect.Sets;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.auth.User;

import java.util.Set;

public abstract class RxBlogUser extends User {
	private final BlogUser blogUserDelegate;
	private static final Set<String> defaultAllowedFields = Sets.newHashSet(
			"username",
			"id"
	);

	public RxBlogUser(BlogUser delegate) {
		super(delegate);
		this.blogUserDelegate = delegate;
	}

	public String getUsername() {
		return blogUserDelegate.getUsername();
	}

	public void setUsername(String username) {
		blogUserDelegate.setUsername(username);
	}

	public String getPassword() {
		return blogUserDelegate.getPassword();
	}

	public void setPassword(String password) {
		blogUserDelegate.setPassword(password);
	}

	public long getId() {
		return blogUserDelegate.getId();
	}

	public void setId(long id) {
		blogUserDelegate.setId(id);
	}

	public JsonObject toJsonObject() {
		return toJsonObject(defaultAllowedFields);
	}

	public JsonObject toJsonObject(Set<String> allowedFields) {
		JsonObject jsonObject = new JsonObject();
		if (allowedFields.contains("username")) {
			jsonObject.put("username", getUsername());
		}
		if (allowedFields.contains("password")) {
			jsonObject.put("password", getPassword());
		}
		if (allowedFields.contains("id")) {
			jsonObject.put("id", getId());
		}
		return jsonObject;
	}
}
