package jb.blog.model.user;

import io.vertx.ext.auth.AbstractUser;

public abstract class BlogUser extends AbstractUser {
	public abstract String getUsername();

	public abstract void setUsername(String username);

	public abstract String getPassword();

	public abstract void setPassword(String password);

	public abstract long getId();

	public abstract void setId(long id);
}
