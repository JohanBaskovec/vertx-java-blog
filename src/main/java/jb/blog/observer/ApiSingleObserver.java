package jb.blog.observer;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.vertx.reactivex.ext.web.RoutingContext;

public class ApiSingleObserver<T> implements SingleObserver<T> {
	private final RoutingContext routingContext;

	public ApiSingleObserver(RoutingContext routingContext) {
		this.routingContext = routingContext;
	}

	@Override
	public void onSubscribe(Disposable d) {

	}

	@Override
	public void onSuccess(T t) {

	}

	@Override
	public void onError(Throwable e) {
		routingContext.fail(e);
	}
}
