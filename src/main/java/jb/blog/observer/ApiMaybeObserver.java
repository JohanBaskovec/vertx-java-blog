package jb.blog.observer;

import io.reactivex.MaybeObserver;
import io.reactivex.disposables.Disposable;
import io.vertx.reactivex.ext.web.RoutingContext;

public class ApiMaybeObserver<T> implements MaybeObserver<T> {
	private final RoutingContext routingContext;

	public ApiMaybeObserver(RoutingContext routingContext) {
		this.routingContext = routingContext;
	}

	@Override
	public void onSubscribe(Disposable d) {

	}

	@Override
	public void onSuccess(T t) {

	}

	@Override
	public void onError(Throwable e) {
		routingContext.fail(e);
	}

	@Override
	public void onComplete() {

	}
}
