package jb.blog.repository.user;

import io.reactivex.Maybe;
import io.reactivex.Single;
import jb.blog.model.user.RxBlogUser;

public interface UserRepository {
	Maybe<RxBlogUser> getOneByUsername(String username);

	Single<RxBlogUser> save(RxBlogUser RxBlogUser);
}
