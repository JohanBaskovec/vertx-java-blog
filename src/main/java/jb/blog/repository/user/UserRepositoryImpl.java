package jb.blog.repository.user;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.sql.SQLConnection;
import jb.blog.model.user.BlogUser;
import jb.blog.model.user.BlogUserImpl;
import jb.blog.model.user.RxBlogUser;
import jb.blog.model.user.RxBlogUserImpl;

import java.util.List;

public class UserRepositoryImpl implements UserRepository {
	private final SQLConnection connection;

	public UserRepositoryImpl(SQLConnection connection) {
		this.connection = connection;
	}

	@Override
	public Maybe<RxBlogUser> getOneByUsername(String username) {
		return connection.rxQueryWithParams(
				"select * from blog_user where username=?",
				new JsonArray().add(username)
		).flatMapMaybe(resultSet -> {
					List<JsonObject> rows = resultSet.getRows();
					if (rows.size() == 0) {
						return Maybe.empty();
					} else {
						JsonObject firstRow = rows.get(0);
						BlogUser user = new BlogUserImpl(
								firstRow.getString("username"),
								firstRow.getString("password"),
								firstRow.getLong("id")
						);

						return Maybe.just(new RxBlogUserImpl(user));
					}
				}
		);
	}

	@Override
	public Single<RxBlogUser> save(RxBlogUser blogUser) {
		if (blogUser.getId() == 0) {
			return connection.rxQueryWithParams(
					"insert into blog_user(id, username, password) values(default, ?, ?) returning id;",
					new JsonArray()
							.add(blogUser.getUsername())
							.add(blogUser.getPassword())
			).flatMap(
					resultSet -> {
						long id = resultSet.getRows().get(0).getLong("id");
						blogUser.setId(id);
						return Single.just(blogUser);
					}
			);
		} else {
			return connection.rxQueryWithParams(
					"update blog_user set username=?, password=? where id=?",
					new JsonArray()
							.add(blogUser.getUsername())
							.add(blogUser.getPassword())
							.add(blogUser.getId())
			).flatMap(
					res -> Single.just(blogUser)
			);
		}
	}

	@Override
	public String toString() {
		return "{\"UserRepositoryImpl\":{"
				+ "                        \"connection\":" + connection + "\n"
				+ "}}";
	}
}
