package jb.blog.repository.article;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.sql.SQLConnection;
import jb.blog.model.article.Article;
import jb.blog.model.article.ArticleImpl;

import java.util.ArrayList;
import java.util.List;

public class ArticleRepositoryImpl implements ArticleRepository {
	private final SQLConnection connection;

	public ArticleRepositoryImpl(SQLConnection connection) {
		this.connection = connection;
	}

	private Article fromRow(JsonObject row) {
		return new ArticleImpl(
				row.getString("title"),
				row.getString("content"),
				row.getLong("id")
		);
	}

	private List<Article> fromRows(List<JsonObject> rows) {
		List<Article> articles = new ArrayList<>();
		for (JsonObject row : rows) {
			articles.add(fromRow(row));
		}
		return articles;
	}

	@Override
	public Single<List<Article>> getAll() {
		return connection.rxQuery("select * from article")
				.flatMap(resultSet -> Single.just(fromRows(resultSet.getRows())));
	}

	@Override
	public Single<List<Article>> findByTitle(String title) {
		return connection.rxQueryWithParams(
				"select * from article where title=?",
				new JsonArray().add(title)
		)
				.flatMap(resultSet -> Single.just(fromRows(resultSet.getRows())));
	}

	@Override
	public Maybe<Article> getOne(long id) {
		return connection.rxQueryWithParams(
				"select * from article where id=?",
				new JsonArray().add(id)
		).flatMapMaybe(resultSet -> {
					List<JsonObject> rows = resultSet.getRows();
					if (rows.size() == 0) {
						return Maybe.empty();
					} else {
						return Maybe.just(fromRow(rows.get(0)));
					}
				}
		);
	}

	@Override
	public Single<Article> save(Article article) {
		if (article.getId() == 0) {
			return connection.rxQueryWithParams(
					"insert into article(id, title, content) values(default, ?, ?) returning id;",
					new JsonArray()
							.add(article.getTitle())
							.add(article.getContent())
			).flatMap(
					resultSet -> {
						long id = resultSet.getRows().get(0).getLong("id");
						article.setId(id);
						return Single.just(article);
					}
			);
		} else {
			return connection.rxQueryWithParams(
					"update article set title=?, content=? where id=?",
					new JsonArray()
							.add(article.getTitle())
							.add(article.getContent())
							.add(article.getId())
			).flatMap(
					res -> Single.just(article)
			);
		}
	}
}
