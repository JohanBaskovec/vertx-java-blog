package jb.blog.repository.article;

import io.reactivex.Maybe;
import io.reactivex.Single;
import jb.blog.model.article.Article;

import java.util.List;

public interface ArticleRepository {
    Single<List<Article>> getAll();

	Single<List<Article>> findByTitle(String title);

    Maybe<Article> getOne(long id);

    Single<Article> save(Article article);
}
