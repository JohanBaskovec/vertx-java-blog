package jb.blog.handler;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.web.RoutingContext;
import jb.blog.model.exception.InvalidUrlParameterException;

/**
 * Attempt to parse the id URL parameter and put it in the routing context data map
 * (it will then be available with routingContext.get("id")
 * <p>
 * If the id isn't a valid id, it will throw an InvalidUrlParameterException.
 */
public class IdParamHandler implements Handler<RoutingContext> {
	@Override
	public void handle(RoutingContext routingContext) {
		String idString = routingContext.request().getParam("id");
		try {
			long id = Long.parseLong(idString);
			if (id <= 0) {
				routingContext.fail(new InvalidUrlParameterException(
						"id",
						idString + " isn't a valid id."
				));
				return;
			}
			routingContext.put("id", id);
			routingContext.next();
		} catch (NumberFormatException e) {
			routingContext.fail(new InvalidUrlParameterException("id", idString + " isn't a number."));
		}
	}
}
