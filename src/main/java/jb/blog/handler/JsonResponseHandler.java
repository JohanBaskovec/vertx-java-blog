package jb.blog.handler;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.web.RoutingContext;

/**
 * Move the "data" data into a json object and reply to the request with it.
 */
public class JsonResponseHandler implements Handler<RoutingContext> {
	@Override
	public void handle(RoutingContext routingContext) {
		Object responseData = routingContext.get("data");
		JsonObject response = new JsonObject();
		if (responseData != null) {
			response.put("data", responseData);
		}
		routingContext.response().end(response.encodePrettily());
	}
}
