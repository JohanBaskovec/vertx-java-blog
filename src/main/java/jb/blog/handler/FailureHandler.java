package jb.blog.handler;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.web.RoutingContext;
import jb.blog.model.exception.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FailureHandler implements Handler<RoutingContext> {
    private static final Logger logger = LoggerFactory.getLogger(FailureHandler.class);

    @Override
    public void handle(RoutingContext context) {
        Throwable throwable = context.failure();
        JsonObject jsonResponse;
        if (throwable instanceof ApiException) {
            ApiException apiException = ((ApiException) throwable);
            jsonResponse = apiException.toErrorJsonObject();
            context.response().setStatusCode(apiException.getHttpCode());
        } else {
            logger.error("Error", throwable);
            jsonResponse = new JsonObject().put("error", "Internal server error.");
            context.response().setStatusCode(500);
        }

        context
                .response()
                .end(jsonResponse.encode());
    }
}
