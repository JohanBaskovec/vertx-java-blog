package jb.blog.handler;

import io.vertx.reactivex.ext.auth.AuthProvider;
import io.vertx.reactivex.ext.auth.User;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.Session;
import jb.blog.security.UserHolder;
import jb.blog.service.Injector;

/**
 * Adapted from Vert.x's default UserSessionHandler
 * <p>
 * The difference is that it uses the "Injector" to get the AuthProvider,
 * and use a reactivex version of UserHolder.
 */
public class UserSessionHandler extends io.vertx.reactivex.ext.web.handler.UserSessionHandler {
	private static final String SESSION_USER_HOLDER_KEY = "__vertx.userHolder";

	public UserSessionHandler() {
		super(null);
	}

	@Override
	public void handle(RoutingContext routingContext) {
		Injector injector = routingContext.get("Injector");
		AuthProvider authProvider = injector.getAuthProvider();

		Session session = routingContext.session();
		if (session != null) {
			User user = null;
			UserHolder holder = session.get(SESSION_USER_HOLDER_KEY);
			if (holder != null) {
				RoutingContext prevContext = holder.context;
				if (prevContext != null) {
					user = prevContext.user();
				} else if (holder.user != null) {
					user = holder.user;
					user.setAuthProvider(authProvider);
					holder.context = routingContext;
					holder.user = null;
				}
				holder.context = routingContext;
			} else {
				// only at the time we are writing the header we should store the user to the session
				routingContext.addHeadersEndHandler(v -> {
					// during the request the user might have been removed
					if (routingContext.user() != null) {
						session.put(SESSION_USER_HOLDER_KEY, new UserHolder(routingContext));
					}
				});
			}
			if (user != null) {
				routingContext.setUser(user);
			}
		}
		routingContext.next();
	}

	@Override
	public String toString() {
		return "{UserSessionHandler}";
	}
}
