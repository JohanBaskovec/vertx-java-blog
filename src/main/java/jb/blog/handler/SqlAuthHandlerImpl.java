package jb.blog.handler;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.auth.User;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.AuthHandler;
import jb.blog.model.exception.ApiException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.HashSet;
import java.util.Set;

public class SqlAuthHandlerImpl implements AuthHandler {
	private final Set<String> authorities = new HashSet<>();

	public SqlAuthHandlerImpl() {
	}

	/**
	 * This always returns null because this class has no "normal" version,
	 * only a reactivex version, so there's no class to delegate to.
	 *
	 * @return null
	 */
	@Override
	public io.vertx.ext.web.handler.AuthHandler getDelegate() {
		return null;
	}

	@Override
	public void handle(RoutingContext routingContext) {
		User user = routingContext.user();
		if (user == null) {
			routingContext.fail(new ApiException() {
				@Override
				public JsonObject toErrorJsonObject() {
					return new JsonObject().put("error", "Please log in.");
				}

				@Override
				public int getHttpCode() {
					return 401;
				}
			});
		} else {
			routingContext.next();
		}
	}

	private void authorizeUser(RoutingContext ctx, User user) {
		authorize(user, authZ -> {
			if (authZ.failed()) {
				processException(ctx, authZ.cause());
				return;
			}
			// success, allowed to continue
			ctx.next();
		});
	}

	private void processException(RoutingContext ctx, Throwable exception) {
		ctx.fail(new ApiException() {
			@Override
			public JsonObject toErrorJsonObject() {
				return new JsonObject().put("error", "An error occurred in auth handler.");
			}
		});
	}

	@Override
	public AuthHandler addAuthority(String authority) {
		authorities.add(authority);
		return this;
	}

	@Override
	public AuthHandler addAuthorities(Set<String> authorities) {
		this.authorities.addAll(authorities);
		return this;
	}

	@Override
	public void parseCredentials(RoutingContext context, Handler<AsyncResult<JsonObject>> handler) {

	}

	@Override
	public void authorize(User user, Handler<AsyncResult<Void>> handler) {
		throw new NotImplementedException();
	}

	@Override
	public String toString() {
		return "{\"SqlAuthHandlerImpl\":{"
				+ "                        \"authorities\":" + authorities + "\n"
				+ "}}";
	}
}
