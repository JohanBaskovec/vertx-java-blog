package jb.blog.handler;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.sql.SQLConnection;
import io.vertx.reactivex.ext.web.RoutingContext;
import jb.blog.service.Injector;
import jb.blog.service.InjectorImpl;

public class InjectorHandler implements Handler<RoutingContext> {
	@Override
	public void handle(RoutingContext context) {
		Injector injector = new InjectorImpl();
		SQLConnection sqlConnection = context.get("SQLConnection");
		if (sqlConnection != null) {
			injector.setSqlConnection(sqlConnection);
		}
		context.put("Injector", injector);
		context.next();
	}
}
