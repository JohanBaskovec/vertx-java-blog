package jb.blog.handler;

import io.vertx.core.Handler;
import io.vertx.reactivex.ext.asyncsql.AsyncSQLClient;
import io.vertx.reactivex.ext.sql.SQLConnection;
import io.vertx.reactivex.ext.web.RoutingContext;
import jb.blog.service.Injector;

public class SqlConnectionHandler implements Handler<RoutingContext> {
    private final AsyncSQLClient sqlClient;

    public SqlConnectionHandler(AsyncSQLClient sqlClient) {
        this.sqlClient = sqlClient;
    }

    @Override
    public void handle(RoutingContext routingContext) {
        sqlClient.getConnection(asyncSqlConnection -> {
            if (asyncSqlConnection.succeeded()) {
                SQLConnection sqlConnection = asyncSqlConnection.result();
                Injector injector = routingContext.get("Injector");
                if (injector != null) {
                    injector.setSqlConnection(sqlConnection);
                }
                routingContext.put("SQLConnection", sqlConnection);
                routingContext.response().endHandler(end -> sqlConnection.close());
                routingContext.next();
            } else {
                System.out.println("Unable to get connection.");
            }
        });
    }
}
