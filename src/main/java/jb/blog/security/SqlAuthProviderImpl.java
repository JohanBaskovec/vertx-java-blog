package jb.blog.security;

import io.reactivex.Single;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.auth.AuthProvider;
import io.vertx.reactivex.ext.auth.User;
import jb.blog.model.exception.InvalidUsernameOrPasswordException;
import jb.blog.repository.user.UserRepository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class SqlAuthProviderImpl extends AuthProvider {
	private final UserRepository userRepository;

	public SqlAuthProviderImpl(UserRepository userRepository) {
		super(null);
		this.userRepository = userRepository;
	}

	@Override
	public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {
		throw new NotImplementedException();
	}

	@Override
	public Single<User> rxAuthenticate(JsonObject authInfo) {
		return userRepository.getOneByUsername(authInfo.getString("username")).flatMapSingle(
				(user) -> {
					if (user.getPassword().equals(authInfo.getString("password"))) {
						return Single.just(user);
					} else {
						return Single.error(new InvalidUsernameOrPasswordException());
					}
				}
		);
	}

	@Override
	public String toString() {
		return "{\"SqlAuthProviderImpl\":"
				+ ",                         \"userRepository\":" + userRepository + "\n"
				+ "}";
	}
}
