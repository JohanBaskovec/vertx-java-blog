package jb.blog.security;


import io.vertx.core.VertxException;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.shareddata.impl.ClusterSerializable;
import io.vertx.ext.web.impl.Utils;
import io.vertx.reactivex.ext.auth.User;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.nio.charset.StandardCharsets;

/**
 * Adapted from {@link io.vertx.ext.web.handler.impl.UserHolder} for reactivex
 */
public class UserHolder implements ClusterSerializable {

	public RoutingContext context;
	public User user;

	public UserHolder() {
	}

	public UserHolder(RoutingContext context) {
		this.context = context;
	}

	@Override
	public void writeToBuffer(Buffer buffer) {
		// try to get the user from the context otherwise fall back to any cached version
		User user = context != null ? context.user() : this.user;
		if (user instanceof ClusterSerializable) {
			buffer.appendByte((byte) 1);
			String className = user.getClass().getCanonicalName();
			if (className == null) {
				throw new IllegalStateException("Cannot serialize " + user.getClass().getName());
			}
			byte[] bytes = className.getBytes(StandardCharsets.UTF_8);
			buffer.appendInt(bytes.length);
			buffer.appendBytes(bytes);
			ClusterSerializable cs = (ClusterSerializable) user;
			cs.writeToBuffer(buffer);
		} else {
			buffer.appendByte((byte) 0);
		}
	}

	@Override
	public int readFromBuffer(int pos, Buffer buffer) {
		byte b = buffer.getByte(pos++);
		if (b == (byte) 1) {
			int len = buffer.getInt(pos);
			pos += 4;
			byte[] bytes = buffer.getBytes(pos, pos + len);
			pos += len;
			String className = new String(bytes, StandardCharsets.UTF_8);
			try {
				Class clazz = Utils.getClassLoader().loadClass(className);
				ClusterSerializable obj = (ClusterSerializable) clazz.newInstance();
				pos = obj.readFromBuffer(pos, buffer);
				user = (User) obj;
			} catch (Exception e) {
				throw new VertxException(e);
			}
		} else {
			user = null;
		}
		return pos;
	}
}

