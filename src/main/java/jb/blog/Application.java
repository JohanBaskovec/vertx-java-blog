package jb.blog;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.handler.LoggerFormat;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.http.HttpServer;
import io.vertx.reactivex.ext.asyncsql.AsyncSQLClient;
import io.vertx.reactivex.ext.asyncsql.PostgreSQLClient;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.*;
import io.vertx.reactivex.ext.web.sstore.LocalSessionStore;
import jb.blog.controller.article.ArticleController;
import jb.blog.controller.article.SessionController;
import jb.blog.form.article.ArticleFormFactoryImpl;
import jb.blog.handler.*;
import jb.blog.handler.UserSessionHandler;
import jb.blog.service.DatabaseReinitializerService;

public class Application extends AbstractVerticle {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	private IdParamHandler idParamHandler;
	private JsonResponseHandler jsonResponseHandler;
	private InjectorHandler injectorHandler;
	private SqlConnectionHandler sqlConnectionHandler;
	private BodyHandler bodyHandler;
	private CookieHandler cookieHandler;
	private SessionHandler sessionHandler;
	private AuthHandler authHandler;
	private UserSessionHandler userSessionHandler;
	private CorsHandler corsHandler;
	private Router mainRouter;

	@Override
	public void start(Future<Void> future) {
		JsonObject postgresClientConfig = new JsonObject()
				.put("host", "localhost")
				.put("port", 5432)
				.put("username", "postgres")
				.put("database", "postgres")
				.put("password", "c4ef37c0fbd747da1c63c0f87d7c62df");
		AsyncSQLClient sqlClient = PostgreSQLClient.createShared(
				vertx,
				postgresClientConfig,
				"PostgresPool1"
		);

		if (Strings.nullToEmpty(System.getProperty("resetDatabase")).equals("true")) {
			sqlClient.rxGetConnection()
					.flatMapCompletable(sqlConnection -> {
						DatabaseReinitializerService databaseReinitializerService;
						databaseReinitializerService = new DatabaseReinitializerService(sqlConnection);
						return databaseReinitializerService.reinitialize();
					})
					.subscribe(new CompletableObserver() {
						@Override
						public void onSubscribe(Disposable d) {
						}

						@Override
						public void onComplete() {
							logger.info("The database has been reinitialized.");
							runHttpServer(future, sqlClient);
						}

						@Override
						public void onError(Throwable e) {
							logger.error(e);
							future.fail(e);
						}
					});
		} else {
			runHttpServer(future, sqlClient);
		}
	}

	private void runHttpServer(Future<Void> future, AsyncSQLClient sqlClient) {
		logger.info("Starting server");
		HttpServerOptions options = new HttpServerOptions()
				.setLogActivity(true)
				.setPort(8092);

		HttpServer server = vertx.createHttpServer(options);

		sqlConnectionHandler = new SqlConnectionHandler(sqlClient);
		bodyHandler = BodyHandler.create();
		cookieHandler = CookieHandler.create();
		sessionHandler = SessionHandler.create(LocalSessionStore.create(vertx));
		authHandler = new SqlAuthHandlerImpl();
		userSessionHandler = new UserSessionHandler();
		idParamHandler = new IdParamHandler();
		jsonResponseHandler = new JsonResponseHandler();
		injectorHandler = new InjectorHandler();

		mainRouter = Router.router(vertx);
		mainRouter.route().handler(LoggerHandler.create(LoggerFormat.DEFAULT));
		corsHandler = CorsHandler.create("http://localhost:4200")
				.allowedMethod(HttpMethod.GET)
				.allowedMethod(HttpMethod.POST)
				.allowedMethod(HttpMethod.PUT)
				.allowedMethod(HttpMethod.DELETE)
				.allowedMethod(HttpMethod.OPTIONS)
				.allowedHeaders(Sets.newHashSet("Content-Type"))
				.allowCredentials(true);
		mainRouter.route().handler(corsHandler);

		initializeArticleRoutes();

		initializeSessionRoutes();

		mainRouter.route().failureHandler(new FailureHandler());

		server.requestHandler(mainRouter::accept).listen(res -> {
			if (res.succeeded()) {
				System.out.println("Server is now listening!");
				future.complete();
			} else {
				System.out.println("Failed to start server!");
				future.fail(res.cause());
			}
		});
	}

	private void initializeSessionRoutes() {
		SessionController controller = new SessionController();
		Router sessionRouter = Router.router(vertx);

		sessionRouter.post("/")
				.handler(bodyHandler)
				.handler(cookieHandler)
				.handler(sqlConnectionHandler)
				.handler(injectorHandler)
				.handler(sessionHandler)
				.handler(userSessionHandler)
				.handler(controller::login)
				.handler(jsonResponseHandler)
		;

		mainRouter.mountSubRouter("/session", sessionRouter);
	}

	private void initializeArticleRoutes() {
		ArticleController controller = new ArticleController(new ArticleFormFactoryImpl());
		Router articleRouter = Router.router(vertx);
		articleRouter
				.get("/")
				.handler(corsHandler)
				.handler(sqlConnectionHandler)
				.handler(injectorHandler)
				.handler(controller::getMultiple)
				.handler(jsonResponseHandler)
		;
		articleRouter
				.get("/:id")
				.handler(corsHandler)
				.handler(idParamHandler)
				.handler(sqlConnectionHandler)
				.handler(injectorHandler)
				.handler(controller::getOne)
				.handler(jsonResponseHandler)
		;
		articleRouter
				.post("/")
				.handler(bodyHandler)
				.handler(corsHandler)
				.handler(sqlConnectionHandler)
				.handler(injectorHandler)
				.handler(cookieHandler)
				.handler(sessionHandler)
				.handler(userSessionHandler)
				.handler(authHandler)
				.handler(controller::createOne)
				.handler(jsonResponseHandler)
		;
		articleRouter
				.put("/:id")
				.handler(bodyHandler)
				.handler(corsHandler)
				.handler(idParamHandler)
				.handler(sqlConnectionHandler)
				.handler(injectorHandler)
				.handler(cookieHandler)
				.handler(sessionHandler)
				.handler(userSessionHandler)
				.handler(authHandler)
				.handler(controller::updateOne)
				.handler(jsonResponseHandler)
		;
		mainRouter.mountSubRouter("/article", articleRouter);
	}
}
