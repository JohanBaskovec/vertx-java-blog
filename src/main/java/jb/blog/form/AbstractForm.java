package jb.blog.form;

import com.google.common.base.Strings;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractForm {
	private final Map<String, String> errors = new HashMap<>();

	protected boolean assertNotEmpty(String fieldName, String fieldValue) {
		if (Strings.isNullOrEmpty(fieldValue)) {
			errors.put(fieldName, fieldName + " should not be empty.");
			return false;
		}
		return true;
	}

	public abstract boolean validate();

	public Map<String, String> getErrors() {
		return errors;
	}
}
