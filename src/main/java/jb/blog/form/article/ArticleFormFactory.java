package jb.blog.form.article;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.web.RoutingContext;

public interface ArticleFormFactory {
    ArticleForm fromRoutingContext(RoutingContext routingContext);

    ArticleForm fromJsonObject(JsonObject json);
}
