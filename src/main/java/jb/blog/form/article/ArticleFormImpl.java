package jb.blog.form.article;

import jb.blog.model.article.Article;
import jb.blog.model.article.ArticleImpl;

public class ArticleFormImpl extends ArticleForm {
	private String title;
	private String content;

	@Override
	public boolean validate() {
		return assertNotEmpty("title", title)
				& assertNotEmpty("content", content);
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getContent() {
		return content;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public Article toArticle() {
		return new ArticleImpl(title, content, 0);
	}
}
