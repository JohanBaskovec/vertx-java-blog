package jb.blog.form.article;

import jb.blog.form.AbstractForm;
import jb.blog.model.article.Article;

public abstract class ArticleForm extends AbstractForm {
    public abstract String getTitle();

    public abstract void setTitle(String title);

    public abstract String getContent();

    public abstract void setContent(String content);

    public abstract Article toArticle();
}
