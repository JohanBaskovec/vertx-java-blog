package jb.blog.form.article;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.web.RoutingContext;

public class ArticleFormFactoryImpl implements ArticleFormFactory {
    @Override
    public ArticleForm fromRoutingContext(RoutingContext routingContext) {
        return fromJsonObject(routingContext.getBodyAsJson());
    }

    @Override
    public ArticleForm fromJsonObject(JsonObject json) {
        ArticleForm form = new ArticleFormImpl();
        form.setTitle(json.getString("title"));
        form.setContent(json.getString("content"));
        return form;
    }
}
