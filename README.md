## Development
### Initial setup
#### Postgresql
* install Docker
* run a postgres Docker image
```
docker run --name postgres -e POSTGRES_PASSWORD=c4ef37c0fbd747da1c63c0f87d7c62df --network="host" postgres:9.6 
```
This will download the postgres image, run it,
create a database called "postgres", 
create a user "postgres" with a password (used exclusively for the 
local development database) and listen to port 5432.

#### Vertx
To run the server in IntelliJ, add a config with main class
```
io.vertx.core.Launcher
```
and program arguments
```
run jb.blog.Application --launcher-class=io.vertx.core.Launcher -conf dev-config.json -DresetDatabase=true
```

--DresetDatabase will drop the blog database and all its table (if they exist), recreate them,
and insert dummy testing data

The server listens to port 8092, try http://localhost:8092/article to
list the test articles.


### Documentation
The API is documented in openapi.yaml using the [OpenAPI Specification](https://github.com/OAI/OpenAPI-Specification).

You can use [Swagger Editor](https://github.com/swagger-api/swagger-editor) to edit it. You can install and 
run it using docker:
```
docker pull swaggerapi/swagger-editor
docker run -d -p 8089:8080 swaggerapi/swagger-editor
```
Go to localhost:8089 and drag and drop openapi.yaml in the editor.

## TODO
* Finish writing openapi.yaml
* Frontend
* Move configuration to yaml file instead of hardcoded
* Docker
